const std = @import("std");
const raylib = @import("raylib");

const GameMode = enum
{
    MainMenu,
    PlayMode,
};

const MainMenuSelction = enum(i32)
{
    SinglePlayer = 0,
    MultiPlayer,
    Controls,
    Quit,

    Count
};

const PlayAgainSelection = enum(i32)
{
    PlayAgain = 0,
    BackToMenu,

    Count
};

const WindowWidth = 800;
const WindowHeight = 457;

var ShowFps = false;
var CurrentGameMode = GameMode.MainMenu;
var CurrentMainMenuSelction = MainMenuSelction.SinglePlayer;
var DoExitGame = false;
var DrawControls = false;
var IsGameOver = false;
var CurrentPlayAgainMenuSelection = PlayAgainSelection.PlayAgain;
var IsMultiplayer = false;

const paddleSpeed: f32 = 300;
var paddleSize = raylib.Vector2{ .x = 10, .y = 125 };

var leftPaddlePos = raylib.Vector2{ .x = 40, .y = 50 };
var pointsPlayerLeft: i32 = 0;

var rightPaddlePos = raylib.Vector2{ .x = WindowWidth - 20 - 40, .y = 50 };
var pointsPlayerRight: i32 = 0;

var ballPos = raylib.Vector2{ .x = 100, .y = 100 };
var ballDir = raylib.Vector2{ .x = 1, .y = -1 };
const ballSpeed: f32 = 400;
const ballRadius: f32 = 10;

var ScoreTextBuffer: [16]u8 = undefined;

pub fn main() !void
{

    raylib.InitWindow(WindowWidth, WindowHeight, "Pong");
    raylib.SetTargetFPS(60);
    defer raylib.CloseWindow();

    while(!raylib.WindowShouldClose() and !DoExitGame)
    {
        const elapsed = raylib.GetFrameTime();

        raylib.BeginDrawing();
        defer raylib.EndDrawing();
        raylib.ClearBackground(raylib.BLACK);

        // NOTE(Fabi): We support toggling FPS in any game mode.
        if(raylib.IsKeyPressed(.KEY_F1))
        {
            ShowFps = !ShowFps;
        }

        switch(CurrentGameMode)
        {
            .MainMenu => { RunMainMenu(); },
            .PlayMode => { try RunGameMode(elapsed); }
        }

        if(ShowFps)
        {
            raylib.DrawFPS(10, 10);
        }
    }
}

fn RunMainMenu() void
{
    DrawCenteredText("Pong", 40, WindowWidth / 2, 20, raylib.WHITE);

    if(raylib.IsKeyPressed(.KEY_S) or raylib.IsKeyPressed(.KEY_DOWN))
    {
        const newIndex = @mod(@intFromEnum(CurrentMainMenuSelction) + 1,
                              @intFromEnum(MainMenuSelction.Count));

        CurrentMainMenuSelction = @enumFromInt(newIndex);
    } 
    else if(raylib.IsKeyPressed(.KEY_W) or raylib.IsKeyPressed(.KEY_UP))
    {
        const newIndex = @mod(@intFromEnum(CurrentMainMenuSelction) - 1,
                              @intFromEnum(MainMenuSelction.Count));

        CurrentMainMenuSelction = @enumFromInt(newIndex);
    }

    if(raylib.IsKeyPressed(.KEY_ENTER))
    {
        if(DrawControls)
        {
            DrawControls = false;
        }
        else 
        {
            switch(CurrentMainMenuSelction)
            {
                .SinglePlayer =>
                {
                    IsMultiplayer = false;
                    CurrentGameMode = .PlayMode;
                    ResetGame();
                    IsGameOver = false;
                },

                .MultiPlayer =>
                {
                    IsMultiplayer = true;
                    CurrentGameMode = .PlayMode;
                    ResetGame();
                    IsGameOver = false;
                },

                .Controls => { DrawControls = true; },
                .Quit => { DoExitGame = true; },
                else => {},
            }
        }
    }

    if(DrawControls)
    {
        DrawCenteredText("Controls", 30, WindowWidth / 2, 100, raylib.WHITE);
        DrawCenteredText("Move Player Left Up/Down: W/S", 20, WindowWidth / 2, 140, raylib.WHITE);
        DrawCenteredText("Move Player Right Up/Down: Arrow Up/Arrow Down", 20, WindowWidth / 2, 180, raylib.WHITE);
        DrawCenteredText("Quit Game: Esc", 20, WindowWidth / 2, 220, raylib.WHITE);
        DrawCenteredText("Back", 30, WindowWidth / 2, 260, raylib.YELLOW);
    }
    else
    {
        var color = if(CurrentMainMenuSelction == .SinglePlayer) raylib.YELLOW else raylib.WHITE;
        DrawCenteredText("Singleplayer", 30, WindowWidth / 2, 100, color);
        color = if(CurrentMainMenuSelction == .MultiPlayer) raylib.YELLOW else raylib.WHITE;
        DrawCenteredText("Multiplayer", 30, WindowWidth / 2, 140, color);
        color = if(CurrentMainMenuSelction == .Controls) raylib.YELLOW else raylib.WHITE;
        DrawCenteredText("Controls", 30, WindowWidth / 2, 180, color);
        color = if(CurrentMainMenuSelction == .Quit) raylib.YELLOW else raylib.WHITE;
        DrawCenteredText("Quit", 30, WindowWidth / 2, 220, color);
        DrawCenteredText("Up/Down: W/S    Accept: Enter", 20, WindowWidth / 2, 300, raylib.WHITE);

    }

    const engineLabel = "Greenhouse Engine Version 0.1.0";
    const engineLabelSize = 10;
    const textWidth = raylib.MeasureText(engineLabel, engineLabelSize);
    const drawX = WindowWidth - textWidth - 10;
    raylib.DrawText(engineLabel, drawX, WindowHeight - 20, engineLabelSize, raylib.WHITE);
}

fn RunGameMode(elapsed: f32) !void
{
    const wasGameOverLastFrame = IsGameOver;
    IsGameOver = pointsPlayerLeft == 10 or pointsPlayerRight == 10;

    if(IsGameOver)
    {
        if(!wasGameOverLastFrame)
        {
            CurrentPlayAgainMenuSelection = .PlayAgain;
        }

        DrawCenteredText("Game is over", 30, WindowWidth / 2, 100, raylib.WHITE);
        if(pointsPlayerLeft > pointsPlayerRight)
        {
            DrawCenteredText("Player One won", 30, WindowWidth / 2, 140, raylib.WHITE);
        }
        else
        {
            DrawCenteredText("Player Two won", 30, WindowWidth / 2, 140, raylib.WHITE);
        }

        if(raylib.IsKeyPressed(.KEY_S) or raylib.IsKeyPressed(.KEY_DOWN))
        {
            const newIndex = @mod(@intFromEnum(CurrentPlayAgainMenuSelection) + 1,
                                @intFromEnum(PlayAgainSelection.Count));

            CurrentPlayAgainMenuSelection = @enumFromInt(newIndex);
        } 
        else if(raylib.IsKeyPressed(.KEY_W) or raylib.IsKeyPressed(.KEY_UP))
        {
            const newIndex = @mod(@intFromEnum(CurrentPlayAgainMenuSelection) - 1,
                                @intFromEnum(PlayAgainSelection.Count));

            CurrentPlayAgainMenuSelection = @enumFromInt(newIndex);
        }

        if(raylib.IsKeyPressed(.KEY_ENTER))
        {
            switch(CurrentPlayAgainMenuSelection)
            {
                .PlayAgain =>
                {
                    ResetGame();
                    IsGameOver = false;
                },
                .BackToMenu => 
                {
                    CurrentGameMode = .MainMenu;
                    ResetGame();
                },
                else => {},
            }
        }
        
        var color = if(CurrentPlayAgainMenuSelection == .PlayAgain) raylib.YELLOW else raylib.WHITE;
        DrawCenteredText("Play Again", 20, WindowWidth / 2, 200, color);
        color = if(CurrentPlayAgainMenuSelection == .BackToMenu) raylib.YELLOW else raylib.WHITE;
        DrawCenteredText("Back to Menu", 20, WindowWidth / 2, 240, color);
    }
    else 
    {
        leftPaddlePos = UpdatePaddlePosition(leftPaddlePos, paddleSize.y,
                                             .KEY_W,
                                             .KEY_S,
                                             elapsed,
                                             paddleSpeed);

        if(IsMultiplayer)
        {
            rightPaddlePos = UpdatePaddlePosition(rightPaddlePos, paddleSize.y,
                                                .KEY_UP,
                                                .KEY_DOWN,
                                                elapsed,
                                                paddleSpeed);    
        }
        else 
        {
            if(ballDir.x > 0)
            {
                var direction: f32 = if(ballDir.y > 0) 1 else -1;

                rightPaddlePos.y += direction * elapsed * paddleSpeed * 0.65;
                rightPaddlePos = rightPaddlePos.clampY(0, WindowHeight - paddleSize.y);
            }
        }

        ballDir = ballDir.normalize();
        var offset = ballDir.scale(ballSpeed * elapsed);
        ballPos.addSet(offset);

        const edgePoint = ballPos.add(ballDir.scale(ballRadius));
        
        const leftPaddlBounds = raylib.Rectangle
        {
            .x = leftPaddlePos.x,
            .y = leftPaddlePos.y,
            .width = paddleSize.x,
            .height = paddleSize.y
        };

        const rightPaddleBounds = raylib.Rectangle
        {
            .x = rightPaddlePos.x,
            .y = rightPaddlePos.y,
            .width = paddleSize.x,
            .height = paddleSize.y
        };

        if(raylib.CheckCollisionCircleRec(ballPos, ballRadius, leftPaddlBounds))
        {
            // NOTE(Fabi): We do this to reasure that after the reflection the ball does not
            // again hit the collider. This could lead to the ball constantly colliding inside
            // the collider leading to the ball appearing as it would slide down the pedal. 
            const hitPointX = leftPaddlePos.x + paddleSize.x;
            ballPos.x = hitPointX + ballRadius;

            ReflectBall(&ballPos, &ballDir, .{ .x = 1, .y = 0 },
                        elapsed,
                        ballSpeed);
        }
        else if(raylib.CheckCollisionCircleRec(ballPos, ballRadius, rightPaddleBounds))
        {
            // NOTE(Fabi): We do this to reasure that after the reflection the ball does not
            // again hit the collider. This could lead to the ball constantly colliding inside
            // the collider leading to the ball appearing as it would slide down the pedal.
            const hitPointX = rightPaddlePos.x;
            ballPos.x = hitPointX - ballRadius;
            
            ReflectBall(&ballPos, &ballDir, .{ .x = -1, .y = 0 },
                        elapsed,
                        ballSpeed);
        }
        else if(edgePoint.y <= 0)
        {
            ReflectBall(&ballPos, &ballDir, .{ .x = 0, .y = 1 },
                        elapsed,
                        ballSpeed);
        }
        else if(edgePoint.y >= WindowHeight)
        {
            ReflectBall(&ballPos, &ballDir, .{ .x = 0, .y = -1 },
                        elapsed,
                        ballSpeed);
        }
        else if(edgePoint.x >= WindowWidth)
        {
            pointsPlayerLeft += 1;
            ballPos = raylib.Vector2{ .x = WindowWidth / 2, .y = WindowHeight / 2 };
            ballDir.x *= -1;
        }
        else if(edgePoint.x <= 0)
        {
            pointsPlayerRight += 1;
            ballPos = raylib.Vector2{ .x = WindowWidth / 2, .y = WindowHeight / 2 };
            ballDir.x *= -1;
        }

        raylib.DrawRectangleV(leftPaddlePos, paddleSize, raylib.BLUE);
        raylib.DrawRectangleV(rightPaddlePos, paddleSize, raylib.RED);
        raylib.DrawCircleV(ballPos, ballRadius, raylib.WHITE);

        // NOTE(Fabi): This how I can format a string in place in a preallocated buffer
        // and convert it into a null terminated string for C additional. It is actually
        // important to append the 0 byte yourself after the operation. 
        const str = try std.fmt.bufPrint(&ScoreTextBuffer, "{d} - {d}",
                                            .{pointsPlayerLeft, pointsPlayerRight});
        ScoreTextBuffer[str.len] = 0;
        const cStr = ScoreTextBuffer[0..str.len: 0];
        
        const textWidth = raylib.MeasureText(cStr, 20);
        const x = (WindowWidth / 2) - @divFloor(textWidth, 2);
        raylib.DrawText(cStr, x, 50, 20, raylib.WHITE);

        DrawCenteredText("Score", 30, WindowWidth / 2, 20, raylib.WHITE);
    }
}

fn UpdatePaddlePosition(postion: raylib.Vector2, paddleheight: f32,
                        upKey: raylib.KeyboardKey,
                        downKey: raylib.KeyboardKey,
                        elapsed: f32,
                        speed: f32) raylib.Vector2
{
    var copy = postion;
    var direction: f32 = 0;

    if(raylib.IsKeyDown(downKey))
    {
        direction += 1;
    }

    if(raylib.IsKeyDown(upKey))
    {
        direction -= 1;
    }

    copy.y += direction * elapsed * speed;
    copy = copy.clampY(0, WindowHeight - paddleheight);

    return copy;
}

fn ReflectBall(position: *raylib.Vector2, direction: *raylib.Vector2,
               normal: raylib.Vector2,
               elapsed: f32,
               speed: f32) void
{
    direction.* = raylib.Vector2Reflect(direction.*, normal);
    direction.* = direction.normalize();

    const offset = direction.scale(speed * elapsed);
    position.addSet(offset);   
}

fn DrawCenteredText(text: [*:0]const u8, fontSize: i32, x: i32, y: i32,
                    color: raylib.Color) void
{
    const textWidth = raylib.MeasureText(text, fontSize);
    const centeredX = x - @divFloor(textWidth, 2);
    raylib.DrawText(text, centeredX, y, fontSize, color);
}

fn ResetGame() void
{
    pointsPlayerLeft = 0;
    pointsPlayerRight = 0;

    leftPaddlePos.y = (WindowHeight / 2) - (paddleSize.y / 2);
    rightPaddlePos.y = (WindowHeight / 2) - (paddleSize.y / 2);

    ballPos.x = WindowWidth / 2;
    ballPos.y = WindowHeight / 2;
}

test "simple test" {
    var list = std.ArrayList(i32).init(std.testing.allocator);
    defer list.deinit(); // try commenting this out and see if zig detects the memory leak!
    try list.append(42);
    try std.testing.expectEqual(@as(i32, 42), list.pop());
}
